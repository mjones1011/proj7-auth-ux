import flask
from flask import Flask, jsonify, request, Response, render_template, session
from flask_restful import Resource, Api, reqparse
import pymongo
from pymongo import MongoClient
import sys
import os
import logging
import csv
import arrow
import acp_times
from password import hash_password, verify_password
from testToken import generate_auth_token, verify_auth_token
from flask_login import LoginManager, login_user, logout_user, login_required, UserMixin, current_user
from flask_wtf import FlaskForm
from flask_wtf.csrf import CSRFProtect
from wtforms import Form, StringField, PasswordField, BooleanField, validators
from wtforms.validators import InputRequired, Length
from bson.objectid import ObjectId
from base64 import b64decode


app = Flask(__name__)
SECRET_KEY = 'SHEmGORhDfCRyisd8N8nEbFA'
app.config['SECRET_KEY'] = SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
db2 = client.userdb

##COPIED IN FROM FLASK_BREVETS.PY

#####
#Sets up homepage
#####
@app.route("/home")
def home():
    db.tododb.delete_many({})
    return render_template('calc.html')

#####
#Calculates times
#####
@app.route("/_calc_times")
def calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request: km")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))

    app.logger.debug("Got a JSON request: distance")
    distance = request.args.get('brevet_distance_km', 999, type=int)
    app.logger.debug("distance={}".format(distance))

    app.logger.debug("Got a JSON request: begin_date")
    begin_date = request.args.get('begin_date', '', type=str)
    app.logger.debug("begin_date={}".format(begin_date))

    app.logger.debug("Got a JSON request: begin_time")
    begin_time = request.args.get('begin_time', '', type=str)
    app.logger.debug("begin_time={}".format(begin_time))

    app.logger.debug("request.args: {}".format(request.args))

    fulltime = begin_date + " " + begin_time
    time = arrow.get(fulltime, 'YYYY-MM-DD HH:mm')

    open_time = acp_times.open_time(km, distance, time)
    close_time = acp_times.close_time(km, distance, time)

    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

#####
#Spits 404
#####
@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("home")
    return render_template('404.html'), 404
##END COPY IN FROM FLASK_BREVETS.PY

@app.route('/display')
def display():
    
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('todo.html', items=items)

@app.route('/new', methods=['POST'])
def new():
    kilometers = request.form.getlist('km')
    starttimes = request.form.getlist('open')
    endtimes = request.form.getlist('close')
    
    for item in range(20):
        if kilometers[item] != '':
            finalinfo = {
                'km': kilometers[item],
                'open_time': starttimes[item],
                'close_time': endtimes[item]
            }
            db.tododb.insert_one(finalinfo)

    return flask.redirect(flask.url_for('home'))

# Laptop Service

api = Api(app)
login_manager = LoginManager()
login_manager.init_app(app)

###
#User
###
class User(UserMixin):
    def __init__(self, user_id):
        self.user_id = user_id
    def is_authenticated(self):
        return True
    def is_active(self):
        return True
    def is_anonymous(self):
        return False
    def get_id(self):
        return self.user_id

@login_manager.user_loader
def load_user(user_id):
    ID = str(user_id)
    user = db2.userdb.find_one(ObjectId(ID))
    app.logger.debug(user)
    if user is None:
        return None
    return User(user['_id'])

###
#FORMS
###
class LoginForm(FlaskForm):
    username = StringField('username', validators=[InputRequired(), Length(min=4, max=16)])
    password = PasswordField('password', validators=[InputRequired(), Length(min=8, max=64)])
    remember = BooleanField('remember me')

class RegisterForm(FlaskForm):
    username = StringField('username', validators=[InputRequired(), Length(min=4, max=16)])
    password = PasswordField('password', validators=[InputRequired(), Length(min=8, max=64)])

###
#Routes
###
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    username = form.username.data
    password = form.password.data
    remember = form.remember.data

    if request.method == 'POST' and form.validate_on_submit():
        user = db2.userdb.find_one({"username":username})
        if user is None:
            flask.flash("User not found")
            return render_template('login.html', form=form)
        if not verify_password(password, user['password']):
            flask.flash("Wrong password")
            return render_template('login.html', form=form)

        userID = str(user['_id'])
        user_obj = User(userID)
        login_user(user_obj, remember=remember)
        
        token = generate_auth_token(expiration=1000)
        session['api_session_token'] = token
        app.logger.debug(token)
        return jsonify({"token":token.decode(), "duration":1000}), 200
        #return flask.redirect(flask.url_for("index"))
    return render_template('login.html', form=form)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return "Logged Out"

@app.route('/api/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        
        if username is None or password is None:
            flask.flash("Empty field(s)!")
            return render_template('register.html', form=form)
        if db.users.find_one({"username":username}) is not None:
            flask.flash("User exists!")
            #If user already exists
            return render_template('register.html', form=form)
    
        hpass = hash_password(password)
        password = None

        post_id = db2.userdb.insert_one({"username":username, "password":hpass})
        userId = str(post_id.inserted_id)

        #login_user(User(userId))
        #return jsonify({"success":"created", "username":username, "location":userId}), 200
        return flask.redirect(flask.url_for("login")), 200
    return render_template('register.html', form=form)

###
#RESOURCES
###
class ListAll(Resource):
    def get(self):
        top = request.args.get('top', 0, type=int)
        if top is not 0:
            rslt = getInfo(top, True, True, sorted="open_time")
        else:
            rslt = getInfo(None, True, True, sorted="open_time") 
        return jsonify(result=rslt)

class ListOpenOnly(Resource):
    def get(self):
        top = request.args.get('top', 0, type=int)
        if top is not 0:
            rslt = getInfo(top, True, False, sorted="open_time")
        else:
            rslt = getInfo(None, True, False, sorted="open_time")
        return jsonify(result=rslt)

class ListCloseOnly(Resource):
    def get(self):
        top = request.args.get('top', 0, type=int)
        if top is not 0:
            rslt = getInfo(top, False, True, sorted="close_time")
        else:
            rslt = getInfo(None, False, True)
        return jsonify(result=rslt)

class ListAllcsv(Resource):
    def get(self):
        top = request.args.get('top', 0, type=int)
        if top is not 0:
            rslt = getInfo(top, True, True)
        else:
            rslt = getInfo(None, True, True)
        
        jsonConv(rslt, True, True)
        csvFile = open('data.csv', 'r')
        return Response(csvFile, mimetype='text/csv')

class ListOpenOnlycsv(Resource):
    def get(self):
        top = request.args.get('top', 0, type=int)
        if top is not 0:
            rslt = getInfo(top, True, False, sorted="open_time")
        else:
            rslt = getInfo(None, True, False, sorted="open_time")

        jsonConv(rslt, True, False)
        csvFile = open('data.csv', 'r')
        return Response(csvFile, mimetype='text/csv')

class ListCloseOnlycsv(Resource):
    def get(self):
        top = request.args.get('top', 0, type=int)
        if top is not 0:
            rslt = getInfo(top, False, True, sorted="close_time")
        else:
            rslt = getInfo(None, False, True, sorted="close_time")

        jsonConv(rslt, False, True)
        csvFile = open('data.csv', 'r')
        return Response(csvFile, mimetype='text/csv')

class getToken(Resource):
    def get(self):
        return 200
#Routes
api.add_resource(ListAll, '/listAll', '/listAll/json')
api.add_resource(ListOpenOnly, '/listOpenOnly', '/listOpenOnly/json')
api.add_resource(ListCloseOnly, '/listCloseOnly', '/listCloseOnly/json')
api.add_resource(ListAllcsv, '/listAll/csv')
api.add_resource(ListOpenOnlycsv, '/listOpenOnly/csv')
api.add_resource(ListCloseOnlycsv, '/listCloseOnly/csv')
api.add_resource(getToken, '/api/token')

#Extra Functions
def getInfo(top, isOpen, isClose, sorted=None):
    fields = 20
    sortBy = "open_time"

    if top is not None:
        fields = top
    if sorted is not None:
        sortBy = sorted

    fullData = db.tododb.find().sort(sortBy, 1).limit(int(fields))
    
    rslt = []
    for item in fullData:
        if isOpen and isClose:
            rslt.append({
                'open': item['open_time'],
                'close': item['close_time'],
                'km': item['km']
            })

        elif isOpen:
            rslt.append({
                'open': item['open_time'],
                'km': item['km']
            })

        else:
            rslt.append({
                'close': item['close_time'],
                'km': item['km']
            })
    
    return rslt

def jsonConv(jObj, isOpen, isClose):
    obj = jObj
    csvFile = open('data.csv', 'w')
    output = csv.writer(csvFile)

    if isOpen and isClose:
        output.writerow(['km', 'open', 'close'])
        for i in obj:
            output.writerow([i['km'], i['open'], i['close']])
    elif isOpen:
        output.writerow(['km', 'open'])
        for i in obj:
            output.writerow([i['km'], i['open']])
    else:
        output.writerow(['km', 'close'])
        for i in obj:
            output.writerow([i['km'], i['close']])
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)
