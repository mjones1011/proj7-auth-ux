# Project 7: Brevet time calculator service
Author: Mason Jones

Contact: masonj@uoregon.edu

Use: To calculate ACP Brevet times.


Instructions:
1) Go to localhost:5000

2) Register a new account

3) Log in with your new account

4) Navigate to localhost:5000/home

5) Choose overall distance.

6) Input checkpoints as km or miles, but DO NOT HIT ENTER. CLICK INTO ANOTHER FIELD TO AUTOCOMPLETE TIMES. HITTING ENTER WILL ACT AS A "SUBMIT" BUTTON.

7) Hit the submit button to enter your data into the database.

8) Hit "Display Times" to see your brevet information.

9) Change to localhost:5000/(listALl, CloseOnly, OpenOnly) to see all of the API sorted data.

10) To see a top # of times, go to localhost:5001/list(All, CloseOnly, OpenOnly)/(format)?top=(number)
BREVET RULES:

While <200km, max speed = 34, min speed = 15

200km < n <= 400km, max speed = 32, min speed = 15

400km < n <= 600km, max speed = 30, min speed = 15

600km < n <= 1000km, max speed = 28, min speed = 11.428

